package com.location.locationfinder.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.location.locationfinder.R;

public class LaunchActivity extends AppCompatActivity {
    ImageView iv;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch);

        iv=(ImageView)findViewById(R.id.ImageView);
      //  Glide.with(this).load(R.drawable.map1).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).crossFade().into(iv);
        MyThread myThread=new MyThread();
        myThread.start();

    }

    class MyThread extends Thread{
        @Override
        public void run() {
            super.run();
            try {
                sleep(7000);

                startActivity(new Intent(LaunchActivity.this,MainActivity.class));
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
