package com.location.locationfinder.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.location.locationfinder.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextInputLayout emailInputLayout, passInputLayout;
    TextInputEditText  emailEditText,passEditText;
    TextView tv_Signup;
    Button btn_Login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        emailEditText = (TextInputEditText) findViewById(R.id.EmailTextInputEditText);
        emailInputLayout = (TextInputLayout) findViewById(R.id.EmailTextInputLayout);

        passEditText = (TextInputEditText) findViewById(R.id.PassTextInputEditText);
        passInputLayout = (TextInputLayout) findViewById(R.id.PassTextInputLayout);

        btn_Login=findViewById(R.id.btn_login);

        tv_Signup = (TextView) findViewById(R.id.sign_up);
        tv_Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
            }
        });

        btn_Login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_login:
                startActivity(new Intent(LoginActivity.this,ScrollingActivity.class));
        }
    }
}
