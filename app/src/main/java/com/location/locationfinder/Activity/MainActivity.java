package com.location.locationfinder.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.location.locationfinder.Adapter.FilterAdapter;
import com.location.locationfinder.Adapter.ItemAdapter;
import com.location.locationfinder.Interface.OnClickItemListner;
import com.location.locationfinder.Adapter.DialogChkboxAdapter;
import com.location.locationfinder.Model.Choice;
import com.location.locationfinder.R;
import com.location.locationfinder.apiLayer.Client;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnClickItemListner,ItemAdapter.ItemListener {
    private EditText et_Search;
    private RecyclerView recyclerView;
    private SeekBar seekBar;
    ArrayList<Choice> choicesList=new ArrayList<>();
    ArrayList<Choice> filterList=new ArrayList<>();

    Context context=MainActivity.this;
    DialogChkboxAdapter customAdapter;
    int progressChangedValue1;
    OnClickItemListner clickItemListner;
    BottomSheetBehavior behavior;
    RecyclerView recyclerView1;
    private ItemAdapter mAdapter;
    TextView tv;
    ImageView iv;
    boolean val=true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        et_Search=(EditText)findViewById(R.id.search);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        seekBar=findViewById(R.id.simSeekBar);
        getData();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);


        for(int i=1;i<20;i++)
        {
            Choice cc=new Choice();
            cc.setId(i);
            cc.setName("CheckBox"+i);
            cc.setValue(false);
            filterList.add(cc);
        }

        Log.e("ResultAlert", "ResultAlert");

        FilterAdapter customAdapter = new FilterAdapter(filterList,MainActivity.this,"vertical");
        recyclerView.setAdapter(customAdapter); // set the Adapter to RecyclerView


        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);


        recyclerView1 = (RecyclerView) findViewById(R.id.recyclerView1);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));


        ArrayList<Choice> items = new ArrayList();
        for(int i=1;i<10;i++)
        {
            Choice cc=new Choice();
            cc.setPlace_name("Place Name"+i);
            cc.setImage(R.drawable.dominos);
            cc.setRating(2);
            items.add(cc);
        }

        mAdapter = new ItemAdapter(items, this);
        recyclerView1.setAdapter(mAdapter);
       // behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

         tv = (TextView) findViewById(R.id.tvshow_bottomsheet);//
        iv = (ImageView) findViewById(R.id.image_updown);
        iv.setImageDrawable(getResources().getDrawable(R.drawable.up));

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(val==true)
                {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    tv.setText("Hide Placed Location");
                    iv.setImageDrawable(getResources().getDrawable(R.drawable.down));


                    val=false;
                }else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    tv.setText("View Placed Location");
                    iv.setImageDrawable(getResources().getDrawable(R.drawable.up));
                    val=true;

                }





            }
        });

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        tv.setText("Hide Placed Location");
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.down));
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                // Toast.makeText(SignUpActivity.this, "Seek bar progress is :" + progressChangedValue,
                //       Toast.LENGTH_SHORT).show();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this, "Radius :" + progressChangedValue+"Km",
                        Toast.LENGTH_SHORT).show();
                progressChangedValue1=progressChangedValue;

            }
        });

}
    public void alertDialog1(){

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.first_dialog);
        dialog.setTitle("Select Choice");
        SeekBar simpleSeekBar=(SeekBar)dialog.findViewById(R.id.simpleSeekBar);
        TextView tv=(TextView)dialog.findViewById(R.id.tv_);
        RecyclerView rv=(RecyclerView)dialog.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(linearLayoutManager);

         customAdapter = new DialogChkboxAdapter(choicesList,MainActivity.this,"vertical");
        rv.setAdapter(customAdapter); // set the Adapter to RecyclerView

        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                // Toast.makeText(SignUpActivity.this, "Seek bar progress is :" + progressChangedValue,
                //       Toast.LENGTH_SHORT).show();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this, "Radius :" + progressChangedValue+"Km",
                        Toast.LENGTH_SHORT).show();
                progressChangedValue1=progressChangedValue;

            }
        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

              //  setData();

            }
        });

        dialog.show();



//        alertDialog=new Dialog(context);
//        AlertDialog.Builder builder = new AlertDialog.Builder ( this );
//        builder.setTitle("Area Of Interest");
//        alertDialog.setContentView(R.btnlay.first_dialog);
//
//        RecyclerView rv=(RecyclerView)alertDialog.findViewById(R.id.recyclerView);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        rv.setLayoutManager(linearLayoutManager);
        //  call the constructor of CustomAdapter to send the reference and data to Adapter




    }
    public void setData()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Client.get_InterestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressBar.setVisibility(View.GONE);
                        Log.e("Response", response);
                     //   Toast.makeText(getApplicationContext(), "Result:- "+response, Toast.LENGTH_SHORT).show();
                        try {
                            JSONObject object = new JSONObject(response);
                            String result=object.getString("result");
                            if(result.equals("1"))
                            {
                                JSONObject dataObject=object.getJSONObject("data");
                                JSONArray jsonArray= dataObject.getJSONArray("interests");

                             for(int t=0;t<jsonArray.length();t++)
                             {

                                 String interest=jsonArray.getString(t);
                                 Choice cc=new Choice();
                                 cc.setName(interest);
                                 choicesList.add(cc);
                                 //customAdapter.notifyDataSetChanged();
                             }

                                alertDialog1();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        //  Log.e("Response1", error.getMessage());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", "setInterest");
              //  params.put("interests", );
              //  params.put("radius",);
                params.put("user_id", "absc1234");

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);  // this = context

        // VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        queue.add(stringRequest);

    }
    public void getData()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Client.get_InterestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressBar.setVisibility(View.GONE);
                        Log.e("Response", response);
                        //   Toast.makeText(getApplicationContext(), "Result:- "+response, Toast.LENGTH_SHORT).show();
                        try {
                            JSONObject object = new JSONObject(response);
                            String result=object.getString("result");
                            if(result.equals("1"))
                            {
                                JSONArray dataArray=object.getJSONArray("data");
                            //    JSONArray jsonArray= dataObject.getJSONArray("interests");

                                for(int t=0;t<dataArray.length();t++)
                                {

                                    JSONObject interest=dataArray.getJSONObject(t);
                                    int in_Id=  interest.getInt("interestId");

                                    String in_Name=  interest.getString("interestName");


                                    Choice cc=new Choice();
                                    cc.setId(in_Id);
                                    cc.setName(in_Name);
                                    choicesList.add(cc);
                                    //customAdapter.notifyDataSetChanged();
                                }

                                alertDialog1();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        //  Log.e("Response1", error.getMessage());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("action", "getInterest");
                //  params.put("password", "parasar");

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);  // this = context

        // VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        queue.add(stringRequest);

    }


    @Override
    public void getFun(ArrayList<Choice> nn) {
    for(int i=0;i<nn.size();i++)
    {
     String List=  nn.get(i).getId()+"  "+ nn.get(i).getName() +"  "+ nn.get(i).isValue();
     Log.e("List Value bhbdb", List);



    }


    }

        @Override
        public void getFilterval(String value) {

        }

    @Override
    public void onItemClick(String item) {
        //setintent
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        tv.setText("View Placed Location");
        iv.setImageDrawable(getResources().getDrawable(R.drawable.up));
        startActivity(new Intent(MainActivity.this,ScrollingActivity.class));

    }




}
