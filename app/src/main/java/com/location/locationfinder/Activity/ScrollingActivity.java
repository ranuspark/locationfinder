package com.location.locationfinder.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.location.locationfinder.Adapter.DialogChkboxAdapter;
import com.location.locationfinder.Adapter.FilterAdapter;
import com.location.locationfinder.Adapter.ItemAdapter;
import com.location.locationfinder.Interface.OnClickItemListner;
import com.location.locationfinder.Model.Choice;
import com.location.locationfinder.R;
import com.location.locationfinder.Utility.GeocodingLocation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScrollingActivity extends AppCompatActivity implements OnMapReadyCallback,OnClickItemListner,ItemAdapter.ItemListener {


    private Menu menu;
    String latlog;
    GoogleMap googleMap;
    AlertDialog alertDialog;
    ArrayList<Choice> choicesList=new ArrayList<>();
    RecyclerView recyclerview;
    ArrayList<Choice> filterList=new ArrayList<>();

    private ItemAdapter mAdapter;
    RecyclerView recyclerview1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deatail1);
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerview1 = (RecyclerView) findViewById(R.id.pro_recyclerView1);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerview.setLayoutManager(linearLayoutManager);


        for(int i=1;i<10;i++)
        {
            Choice cc=new Choice();
            cc.setId(i);
            cc.setName("Filter  "+i);
            filterList.add(cc);
        }

        Log.e("ResultAlert", "ResultAlert");

        FilterAdapter customAdapter = new FilterAdapter(filterList,ScrollingActivity.this,"vertical");
        recyclerview.setAdapter(customAdapter); // set the Adapter to RecyclerView


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                     //   .setAction("Action", null).show();

                String q="Tower-D, Plot No. 5,5th Floor, Logix Techno Park,Ground Floor, Timmy Arcade,Sector 127, Noida - 201304, U.P.";

                GeocodingLocation locationAddress = new GeocodingLocation();
                locationAddress.getAddressFromLocation(q,
                        getApplicationContext(), new GeocoderHandler());
                // Uri gmmIntentUri = Uri.parse("google.navigation:q="+q+",+U.P.+India");
//            Uri gmmIntentUri = Uri.parse("google.navigation:q=Timmy Arcade,Sector 127, Noida - 201304+U.P.,+Noida+U.P.");
//            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//            mapIntent.setPackage("com.google.android.apps.maps");
//            startActivity(mapIntent);


                Uri gmmIntentUri = Uri.parse("google.navigation:q="+latlog);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                    showOption(R.id.action_info);
                } else if (isShow) {
                    isShow = false;
                    hideOption(R.id.action_info);
                }
            }
        });

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map1);

        // Getting GoogleMap object from the fragment

        fm.getMapAsync(this);


        recyclerview1.setHasFixedSize(true);
        recyclerview1.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Choice> items = new ArrayList();
        for(int i=1;i<10;i++)
        {
            Choice cc=new Choice();
            cc.setPlace_name("Place Name"+i);
            cc.setImage(R.drawable.dominos);
            cc.setRating(2);
            items.add(cc);
        }

        mAdapter = new ItemAdapter(items, this);
        recyclerview1.setAdapter(mAdapter);

        // LatLng object to store user input coordinates



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
       hideOption(R.id.action_info);
       // showOption(R.id.action_info);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_info) {
            Toast.makeText(this, "Navigate", Toast.LENGTH_LONG).show();
            String q="Tower-D, Plot No. 5,5th Floor, Logix Techno Park,Ground Floor, Timmy Arcade,Sector 127, Noida - 201304, U.P.";

            GeocodingLocation locationAddress = new GeocodingLocation();
            locationAddress.getAddressFromLocation(q,
                    getApplicationContext(), new GeocoderHandler());
           // Uri gmmIntentUri = Uri.parse("google.navigation:q="+q+",+U.P.+India");
//            Uri gmmIntentUri = Uri.parse("google.navigation:q=Timmy Arcade,Sector 127, Noida - 201304+U.P.,+Noida+U.P.");
//            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//            mapIntent.setPackage("com.google.android.apps.maps");
//            startActivity(mapIntent);


            Uri gmmIntentUri = Uri.parse("google.navigation:q="+latlog);
           Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hideOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap = googleMap;
        googleMap.clear();
        Double lat=28.595184; //28.595184
        Double lng=77.315566;//315566
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng points = new LatLng(lat,lng);
//        googleMap.addMarker(new MarkerOptions().position(points).title("Domino's"));
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(points));

        Marker ham = googleMap.addMarker(new MarkerOptions().position(points).title("Domino's Pizza"));
//        googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(lat,lng))
//                .title("Facebook")
//                .snippet("Facebook HQ: Menlo Park"));

        ham.showInfoWindow();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points,18.0F));


    }

    @Override
    public void getFun(ArrayList<Choice> nn) {

    }

    @Override
    public void getFilterval(String value) {

    }

    @Override
    public void onItemClick(String item) {

    }


    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    latlog = bundle.getString("address");
                    break;
                default:
                    latlog = null;
            }
            Log.e("Address", latlog);
            //latLongTV.setText(locationAddress);
        }
    }






}
