package com.location.locationfinder.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.location.locationfinder.Adapter.DialogChkboxAdapter;
import com.location.locationfinder.Model.Choice;
import com.location.locationfinder.R;

import java.util.ArrayList;

public class SignUpActivity extends AppCompatActivity {
    TextInputLayout nameInputLayout, emailInputLayout, mobileInputLayout, passInputLayout;//, conpassInputLayout;
    TextInputEditText  nameEditText, emailEditText, mobileEditText,passEditText;//,conpassEditText;
    Button btn_Login;
    ArrayList<Choice>choicesList=new ArrayList<>();
  //  Dialog alertDialog;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup1);

        nameEditText = (TextInputEditText) findViewById(R.id.NameTextInputEditText);
        nameInputLayout = (TextInputLayout) findViewById(R.id.NameTextInputLayout);

        emailEditText = (TextInputEditText) findViewById(R.id.EmailTextInputEditText);
        emailInputLayout = (TextInputLayout) findViewById(R.id.EmailTextInputLayout);

        mobileEditText = (TextInputEditText) findViewById(R.id.MobileTextInputEditText);
        mobileInputLayout = (TextInputLayout) findViewById(R.id.MobileTextInputLayout);

        passEditText = (TextInputEditText) findViewById(R.id.PassTextInputEditText);
        passInputLayout = (TextInputLayout) findViewById(R.id.PassTextInputLayout);

//        conpassEditText = (TextInputEditText) findViewById(R.id.ConPassTextInputEditText);
//        conpassInputLayout = (TextInputLayout) findViewById(R.id.ConPassTextInputLayout);

        btn_Login = (Button) findViewById(R.id.btn_login);

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
            }
        });

//        alertDialog1();

    }
    /*public void alertDialog1(){

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.first_dialog);
        dialog.setTitle("Select Choice");
        SeekBar simpleSeekBar=(SeekBar)dialog.findViewById(R.id.simpleSeekBar);
        TextView tv=(TextView)dialog.findViewById(R.id.tv_);
        RecyclerView rv=(RecyclerView)dialog.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(linearLayoutManager);

//        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
//        // if button is clicked, close the custom dialog
//        dialogButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });


        for(int i=1;i<20;i++)
        {
            Choice cc=new Choice();
            cc.setId(i);
            cc.setName("CheckBox"+i);
            choicesList.add(cc);
        }

        Log.e("ResultAlert", "ResultAlert");

        DialogChkboxAdapter customAdapter = new DialogChkboxAdapter(choicesList,SignUpActivity.this,"vertical");
        rv.setAdapter(customAdapter); // set the Adapter to RecyclerView

        simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
               // Toast.makeText(SignUpActivity.this, "Seek bar progress is :" + progressChangedValue,
                 //       Toast.LENGTH_SHORT).show();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(SignUpActivity.this, "Radius :" + progressChangedValue+"Km",
                        Toast.LENGTH_SHORT).show();
            }
        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();




//        alertDialog=new Dialog(context);
//        AlertDialog.Builder builder = new AlertDialog.Builder ( this );
//        builder.setTitle("Area Of Interest");
//        alertDialog.setContentView(R.btnlay.first_dialog);
//
//        RecyclerView rv=(RecyclerView)alertDialog.findViewById(R.id.recyclerView);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
//        rv.setLayoutManager(linearLayoutManager);
        //  call the constructor of CustomAdapter to send the reference and data to Adapter




    } */

}
