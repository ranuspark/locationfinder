package com.location.locationfinder.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;


import com.location.locationfinder.Interface.OnClickItemListner;
import com.location.locationfinder.Model.Choice;
import com.location.locationfinder.R;

import java.util.ArrayList;


public class DialogChkboxAdapter extends RecyclerView.Adapter<DialogChkboxAdapter.MyViewHolder>  {

    private ArrayList<Choice> choiceList;
    private Context context;
    private String orientation;
    public OnClickItemListner mCallback;

    public DialogChkboxAdapter(ArrayList<Choice> choiceList, Context context, String orientation) {
        this.choiceList = choiceList;
        this.context = context;
        this.orientation = orientation;
        this.mCallback =(OnClickItemListner) context;
//        setHasStableIds(true);
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       // int btnlay = "landscape".equals(orientation) ? R.btnlay.chat_entry_landscape : R.btnlay.chat_entry_portrait;
        int layout=R.layout.dialog_checkbox;
         // int layout=R.layout.dialog_chip;

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new DialogChkboxAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DialogChkboxAdapter.MyViewHolder holder, int position) {
        final Choice chatEntry = choiceList.get(position);

        holder.chkbox.setText(chatEntry.getName());
        Log.e("ResultAlert", "ResultAlert"+"Hello");
        ArrayList<Choice>ll=new ArrayList<>();
        holder.chkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Choice cc;
               if(holder.chkbox.isChecked())
               {
                   cc= new Choice(chatEntry.getId(),chatEntry.getName(), true);
               }else {
                   cc= new Choice(chatEntry.getId(),chatEntry.getName(), false);
               }
             ll.add(cc);
    mCallback.getFun(ll);
            }
        });

     //   holder.chip.setText(chatEntry.getName());


    }

    @Override
    public int getItemCount() {
        return choiceList.size();
    }

/*    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox chkbox;
       // public Chip chip;


        public MyViewHolder(View view) {
            super(view);
            chkbox = (CheckBox) view.findViewById(R.id.checkBox);
           // chip = (Chip) view.findViewById(R.id.chip13);
        }
    }
}