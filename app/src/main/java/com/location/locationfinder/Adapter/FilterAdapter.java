package com.location.locationfinder.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.location.locationfinder.Interface.OnClickItemListner;
import com.location.locationfinder.Model.Choice;
import com.location.locationfinder.R;

import java.util.ArrayList;


public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder>  {

    private ArrayList<Choice> choiceList;
    private Context context;
    private String orientation;
    public  OnClickItemListner mCallback;
    boolean val=true;

    public FilterAdapter(ArrayList<Choice> choiceList, Context context, String orientation) {
        this.choiceList = choiceList;
        this.context = context;
        this.orientation = orientation;
        this.mCallback = (OnClickItemListner) context;
//        setHasStableIds(true);
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       // int btnlay = "landscape".equals(orientation) ? R.btnlay.chat_entry_landscape : R.btnlay.chat_entry_portrait;
        int layout=R.layout.filter_adap;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new FilterAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final FilterAdapter.MyViewHolder holder, int position) {
        final Choice chatEntry = choiceList.get(position);
        ArrayList<Choice>ll=new ArrayList<>();

        holder.tv_Name.setText(chatEntry.getName());
        Log.e("ResultAlert", "ResultAlert"+"Hello");
        holder.tv_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(val==false) {
                    holder.tv_Name.setBackgroundColor(Color.parseColor("#02b7c2"));
                    holder.tv_Name.setTextColor(Color.parseColor("#FFFFFF"));
                    val=true;
                }else {

                    holder.tv_Name.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    holder.tv_Name.setTextColor(Color.parseColor("#02b7c2"));
                    val=false;
                }

                Log.e(String.valueOf(context),choiceList.get(position).getName()+"  "+choiceList.get(position).getId());
                Log.e(String.valueOf(context),holder.tv_Name.getText().toString()+"Hello guys");

     //   mCallback.getFilterval(holder.tv_Name.getText().toString()); //obj.getNot_id()


            }
        });

    }

    @Override
    public int getItemCount() {
        return choiceList.size();
    }

/*    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_Name;


        public MyViewHolder(View view) {
            super(view);
            tv_Name = (TextView) view.findViewById(R.id.tv_name);
            tv_Name.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tv_Name.setTextColor(Color.parseColor("#02b7c2"));
        }
    }
}