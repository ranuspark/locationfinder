package com.location.locationfinder.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.location.locationfinder.Model.Choice;
import com.location.locationfinder.R;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private ArrayList<Choice> mItems;
    private ItemListener mListener;
    private Context context;

 public ItemAdapter(ArrayList<Choice> items, Context listener) {
        mItems = items;
        mListener = (ItemListener)listener;
        context=listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sheetlay, parent, false));
    }

    @Override
    public void onBindViewHolder(final ItemAdapter.ViewHolder holder, int position) {
       // holder.setData(mItems.get(position).toString());

        holder.tv_Name.setText(mItems.get(position).getPlace_name());
        holder.Iv.setImageDrawable(context.getDrawable(mItems.get(position).getImage()));
        holder.rating_Bar.setRating(2.0f);


    }



    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textView,tv_Name;
        String item;
        ImageView Iv;
        RatingBar rating_Bar;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView = (TextView) itemView.findViewById(R.id.textView);//iv
            tv_Name = (TextView) itemView.findViewById(R.id.tv_name);
            Iv = (ImageView) itemView.findViewById(R.id.iv);
            rating_Bar = (RatingBar) itemView.findViewById(R.id.ratingBar);
        }

        void setData(String item) {
            this.item = item;
            textView.setText(item);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(String item);
    }
}
