package com.location.locationfinder.Model;

import android.widget.RatingBar;

public class Choice {
    private  int id;
    private  String name;
    private boolean value;

    private  String place_name;
    private int image;
    private int rating;

    public Choice()
    {

    }
    public Choice(int id,String name,boolean value)
    {
        this.id=id;
        this.name=name;
       this.value=value;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
